from django.urls import path
from productos.views import ListadoProductos, CrearProducto, ModificarProducto, DetalleProducto, CrearProveedor, ListadoProveedores, \
    ModificarProveedor, DetalleProveedor, CrearCompra, ModificarCompra, ListadoCompras

prodcutos_patterns = ([
    path('', ListadoProductos.as_view(), name="listado_productos"),
    path('proveedores/', ListadoProveedores.as_view(), name="listado_proveedores"),
    path('crear_producto/', CrearProducto.as_view(), name="crear_producto"),
    path('modificar_producto/(?P<pk>.+)/',ModificarProducto.as_view(), name="modificar_producto"),
    path('detalle_producto/(?P<pk>.+)/',DetalleProducto.as_view(), name="detalle_producto"),
    path('crear_proveedor/', CrearProveedor.as_view(), name="crear_proveedor"),
    path('modificar_proveedor/(?P<pk>.+)/',ModificarProveedor.as_view(), name="modificar_proveedor"),
    path('detalle_proveedor/(?P<pk>.+)/',DetalleProveedor.as_view(), name="detalle_proveedor"),
    path('crear_compra/', CrearCompra.as_view(), name="crear_compra"),
    path('modificar_compra/(?P<pk>.+)/',ModificarCompra.as_view(), name="modificar_compra"),
    path('compras/', ListadoCompras.as_view(), name="listado_compras"),
], 'productos')
